
# Ejercicio 16.2.-
# Programa para calcular los factoriales de los numeros entre el 1 y 10
def calcular_factorial(num):
    if num == 0 or num == 1:
        return 1
    else:
        return num*calcular_factorial(num-1)

for i in range(1,11):
    resultado = calcular_factorial(i)
    print('El factorial de', i, 'es', resultado)